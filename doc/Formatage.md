# Formatage des datas:
Champs                      Type Champs     Transformation du champs
Horodateur                  object          colonne à supprimer
[Q1.1] Prénom               object          Lower_case
[Q1.2] Nom                  object          Lower_case
[Q1.3] Pays de résidence    object          
[Q2.1] Voyage               object          si non je supprime la ligne
[Q3.1] Pays                 object          
[Q3.2] Durée du voyage      object          INT + transforme float en int + si pas de durée je supprime la ligne
Nom des champs              STR             Retrait des crochets (questions)