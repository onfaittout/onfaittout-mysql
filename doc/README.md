# Samplon-Exo_G Form-Mysql-FastAPI-Dash

Contexte du projet

On veut faire un sondage sur des données de voyage.

Suivre le modèle des données.

# 1 - Créer un Google Form qui permet d'interroger les gens sur les voyages qu’ils ont effectués dans leur vie.
*On veut le nom des répondants et leur pays d’origine.*

On veut savoir quel pays a été visité et combien de jours ont été passés dans le pays en question.

On s'intéresse uniquement aux pays européens.

Une fois le formulaire prêt, partagez son adresse et répondez-y plusieurs fois pour tester les réponses.
​

# 2 - Utilisez l’API de Google sheets pour récupérer les réponses au formulaire à partir d’un script python.

Insérer les données dans une base de données mysql.


# 3 - Créer une API avec FastAPI qui permet d'accéder aux données de la base mysql.
On veut :

Une route qui permet de récupérer les données en fonction du pays.

Une route qui permet de récupérer les données en fonction de la personne.

Une route qui permet de récupérer toutes les données avec une limite et un décalage (aka. limit, offset)

Une route qui envoie des stats (pays le plus visité, moyenne de la durée des visites par pays, personne ayant le plus voyagé etc.)


# 4 - Créer une application avec Dash qui récupère les données à partir de l’API FastAPI.
On veut les vues suivantes :

Une carte de l’Europe, couleurs en fonction du nombre de jours total passés dans chaque pays

Une carte de l'Europe, couleurs en fonction du nombre de personne qui ont voyagé dans le pays.

Une carte de l’Europe avec une ligne entre chaque pays d’origine et chaque pays visité. L'épaisseur de la ligne doit dépendre du nombre de personnes qui ont effectué le voyage.

Une liste des pays les plus visités (avec nombre de visites) en fonction des pays d'origine des utilisateurs. (on choisit le pays dans un dropdown).

Une vue qui affiche les statistiques fournies par l'API.