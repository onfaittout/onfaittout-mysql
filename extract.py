import re

import os
import mysql.connector
import pandas as pd
import unidecode
from sqlalchemy import create_engine


def insert_pays(engine):
    df = pd.read_excel("data/list_pays.xlsx")
    print(df)
    df.index += 1
    df.to_sql(name="pays", con=engine, if_exists="append", index_label="id_pays")


def get_spreadsheet_as_csv():
    sheet_id = "13JC_KQ_aZVf9YfoKDuxxoRFjn0HyxvoegTkHAbvIMUI"
    return pd.read_csv(f"https://docs.google.com/spreadsheets/d/{sheet_id}/export?format=csv")


def get_id(x):
    # google_voyages
    con = mysql.connector.connect(
            user=os.environ["DB_USER"],
            password=os.environ["DB_PASS"],
            database=os.environ["DB_DB"],
            host=os.environ["DB_HOST"]
        )

    cursor = con.cursor(dictionary=True)
    print(x)
    cursor.execute("""SELECT id_pays FROM pays WHERE pays like LOWER(%s) LIMIT 1""", [x])
    result = cursor.fetchone()
    cursor.close()
    con.close()
    if result:
        return int(result['id_pays'])
    return None


def if_number(x):
    try:
        return int(float(x))
    except:
        return None


if __name__ == '__main__':
    engine = create_engine(f'mysql+pymysql://{os.environ["DB_USER"]}:{os.environ["DB_PASS"]}@{os.environ["DB_HOST"]}/{os.environ["DB_DB"]}', echo=False)
    insert_pays(engine)
    df = get_spreadsheet_as_csv()
    df.rename(columns={k: unidecode.unidecode(re.search(r"(\w+\s?)+?$", k.lower()).group(0)).replace(" ", "_") for k in
                       df.columns}, inplace=True)
    df.rename(columns={"pays_visite": "pays_name", "duree_du_voyage": "duree", "pays_residence": "pays_residence_name"},
              inplace=True)
    df.drop(columns=["horodateur"], inplace=True)
    df.dropna(subset="pays_name", inplace=True)
    df.index += 1
    df_personne = df[["pays_residence_name", "nom", "prenom", "email"]].groupby(["nom", "prenom", "pays_residence_name", "email"]).apply(
        lambda x: list(x.index))
    print(df_personne)
    df_personne.name = "id_from_df"
    df_personne = df_personne.reset_index()
    df_personne.index += 1
    df_personne["pays_residence_name"] = df_personne["pays_residence_name"].apply(
        lambda x: re.search(r"([a-z-]+\s?)+?$", x.lower()).group(0))
    df_personne["pays_residence"] = df_personne["pays_residence_name"].apply(get_id)
    df["pays_name"] = df["pays_name"].apply(lambda x: re.search(r"([a-z-]+\s?)+?$", x.lower()).group(0))
    df["pays"] = df["pays_name"].apply(get_id)
    df_personne = df_personne.dropna(subset="pays_residence")
    df = df.dropna(subset="pays")
    df_personne[["nom", "prenom", "pays_residence", "email"]].to_sql(name="personnes", con=engine, index_label="id_personne",
                                                                if_exists="append")
    df_personne = df_personne.explode("id_from_df")
    df_personne.reset_index(inplace=True)
    df.reset_index(inplace=True)
    df_voyage = df_personne.merge(df, how="inner", left_on="id_from_df", right_on="index")[["index_x", "pays", "duree"]]
    df_voyage.rename(columns={"index_x": "id_personne", "pays": "id_pays"}, inplace=True)
    print(df_voyage["duree"])
    df_voyage.to_sql("voyages", engine, if_exists="append", index=False)
