SCRIPT_DIR=$(dirname $(realpath "$0"))
if [ -f "$SCRIPT_DIR/.env" ]; then
  export $(cat $SCRIPT_DIR/.env | xargs)
fi

if [ -z "${DB_USER}" ] || [ -z "${DB_PASS}" ] || [ -z "${DB_HOST}" || -z "${DB_DB}"]; then
  if [ -z "${DB_USER}" ]; then
    read -p 'database username: ' DB_USER
  fi

  if [ -z "${DB_PASS}" ]; then
    read -sp 'database password: ' DB_PASS
    echo
  fi

  if [ -z "${DB_HOST}" ]; then
    read -p 'database host: ' DB_HOST
  fi

  if [ -z "${DB_DB}" ]; then
    read -p 'database name: ' DB_DB
  fi

  echo "DB_HOST=$DB_HOST" > .env
  echo "DB_USER=$DB_USER" >> .env
  echo "DB_PASS=$DB_PASS" >> .env
  echo "DB_DB=$DB_DB" >> .env
fi


python3 -m venv "$SCRIPT_DIR/.venv"
source "$SCRIPT_DIR/.venv/bin/activate"
pip install -r "$SCRIPT_DIR/requirements.txt"
deactivate
