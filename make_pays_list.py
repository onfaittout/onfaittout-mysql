import pandas as pd

if __name__ == "__main__":
    df_code_pays = pd.read_csv("./data/pays_translate.csv", header=None)
    print(df_code_pays)
    df_code_pays = df_code_pays[[2,4]]
    df_code_pays[4] = df_code_pays[4].apply(lambda x: x.lower())
    df_pays_ue = pd.read_json("./data/pays.json")
    df_pays_ue[0] = df_pays_ue[0].apply(lambda x: x.lower())
    df_pays_ue=df_pays_ue.merge(df_code_pays, left_on=0, right_on=4)

    # Access built-in Natural Earth data via GeoPandas

    # Get a list (dataframe) of country centroids
    centroids = pd.read_csv("https://cdn.jsdelivr.net/gh/gavinr/world-countries-centroids@v1/dist/countries.csv")
    df_pays_ue = df_pays_ue.merge(centroids, left_on=2, right_on= "ISO")[[2, 4, "longitude", "latitude"]]
    print(df_pays_ue)
    df_pays_ue.rename(columns={2:"code", 4:"name", "longitude":"lon", "latitude":"lat"}, inplace=True)
    df_pays_ue.drop_duplicates(subset="name", keep="first", inplace=True)
    df_pays_ue.set_index("name", inplace=True)
    with open("./data/data_pays_ue.json", "w", encoding="utf-8") as file:
        df_pays_ue.to_json(file, force_ascii=False, orient="index")



    

    
