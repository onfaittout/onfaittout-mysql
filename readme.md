# on fait tout, la base de donnees

## installation

```sh
git clone https://gitlab.com/onfaittout/onfaittout-mysql.git
cd onfaittout-mysql

# mise en place de la bdd
sudo mysql < scripts/init.py

# installation des package necessaire et mise en place du venv
# renseigner les infos pour l'acces a la bdd
chmod +x install.sh
./install.sh

# generer les fichier et extraire les donnees du google sheet
./run.sh 
```
