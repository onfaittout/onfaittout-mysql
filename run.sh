SCRIPT_DIR=$(dirname $(realpath "$0"))
cd "$SCRIPT_DIR"
if [ -f "$SCRIPT_DIR/.env" ]; then
  export $(cat $SCRIPT_DIR/.env | xargs)
  if [ -z "${DB_USER}" ] || [ -z "${DB_PASS}" ] || [ -z "${DB_HOST}" ] || [ -z "${DB_DB}" ]; then
    echo "run install.sh before running the api"
    exit 1
  fi
else
  echo "run install.sh before running the api"
fi

source "$SCRIPT_DIR/.venv/bin/activate"
python "$SCRIPT_DIR/make_pays_list.py"
python "$SCRIPT_DIR/extract.py"
deactivate
